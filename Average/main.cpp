#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <vector>
using namespace std;


void read(fstream &F,vector< unsigned long> &sum,vector<int> &ii, bool &end_of_read, int batch_size,mutex &mtx)
{
    while (!F.eof())
    {
        unsigned long sum_read=0;
        int ii_read;
        for(ii_read=0; (ii_read<batch_size)and(!F.eof());ii_read++)
        {
            unsigned int a;
            F>>a;
            sum_read+=a;

        }
        mtx.lock();
        sum.push_back(sum_read);
        ii.push_back(ii_read);
        mtx.unlock();
    }
    end_of_read=true;
}

void write(vector<unsigned long> &sum,vector<int> &ii,bool &end_of_read, mutex &mtx)
{
    unsigned long h=0;
    double aver;
    int ii_write;
    unsigned long sum_write;
    bool first = true;
    while(!sum.empty() or !end_of_read)
    {
        if (!sum.empty())
        {

            mtx.lock();
            sum_write=sum[0];
            ii_write=ii[0];
            sum.erase(sum.begin());
            ii.erase(ii.begin());
            mtx.unlock();


            h+=ii_write;
            if (first)
            {
                aver=double(sum_write)/h;
                first=false;
            }
            else aver=aver*((h-ii_write)/double(h))+double(sum_write)/h;
            cout <<aver<< endl;
        }
    }
}

int main()
{
    string path="file.txt";
    fstream F;
    F.open(path);
    if(F)
    {
        mutex mtx;
        int batch_size=5;
        bool end_of_read=false;
        vector<unsigned long> sum;
        vector<int> ii;
        thread t1(read, ref(F),ref(sum),ref(ii), ref(end_of_read),batch_size, ref(mtx));
        thread t2(write,ref(sum), ref(ii), ref(end_of_read), ref(mtx));
        t1.join();
        t2.join();

        cout <<"End of calculating"<< endl;
    }
    else cout <<"Error"<< endl;
    F.close();
    return 0;
}
